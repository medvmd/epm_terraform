resource "aws_iam_group" "group" {
  name = "${var.student_name}-${var.student_surname}-01-group"

}

data "template_file" "s3_policy" {
  template = file("${path.module}/write-to-epam-tf-lab-policy.json.tpl")

  vars = {
    bucket_name = aws_s3_bucket.epam_tf_lab.bucket
  }
}

resource "aws_iam_policy" "write_to_epam_tf_lab_policy" {
  name        = "write-to-epam-tf-lab"
  description = "Allows write access to epam-tf-lab S3 bucket"
  policy      = data.template_file.s3_policy.rendered

  tags = {
    Terraform = "true"
    Project   = "epam-tf-lab"
    Owner     = "${var.student_name}_${var.student_surname}"
  }
}

resource "aws_iam_role" "ec2_role" {
  name               = "ec2-role-for-epam-tf-lab"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF

  tags = {
    Terraform = "true"
    Project   = "epam-tf-lab"
    Owner     = "${var.student_name}_${var.student_surname}"
  }
}

resource "aws_iam_role_policy_attachment" "attach_policy_to_role" {
  role       = aws_iam_role.ec2_role.name
  policy_arn = aws_iam_policy.write_to_epam_tf_lab_policy.arn
}

resource "aws_iam_instance_profile" "ec2_instance_profile" {
  name = "ec2-instance-profile-for-epam-tf-lab"
  role = aws_iam_role.ec2_role.name

  tags = {
    Terraform = "true"
    Project   = "epam-tf-lab"
    Owner     = "${var.student_name}_${var.student_surname}"
  }
}
