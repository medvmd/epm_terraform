locals {
  vpc_name         = "${var.student_name}-${var.student_surname}-01-vpc"
  subnet_public_a  = "${var.student_name}-${var.student_surname}-01-subnet-public-a"
  subnet_public_b  = "${var.student_name}-${var.student_surname}-01-subnet-public-b"
  subnet_public_c  = "${var.student_name}-${var.student_surname}-01-subnet-public-c"
  internet_gateway = "${var.student_name}-${var.student_surname}-01-igw"
  route_table      = "${var.student_name}-${var.student_surname}-01-rt"
  tags = {
    Terraform = "true"
    Project   = "epam-tf-lab"
    Owner     = "${var.student_name}_${var.student_surname}"
  }
}
