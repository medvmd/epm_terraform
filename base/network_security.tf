resource "aws_security_group" "ssh_inbound" {
  name        = "ssh-inbound"
  description = "Allows SSH access from safe IP range"
  vpc_id      = aws_vpc.main.id

  tags = {
    Terraform = "true"
    Project   = "epam-tf-lab"
    Owner     = "${var.student_name}_${var.student_surname}"
    Name      = "ssh_inbound"
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["185.115.7.7/32"]
  }
}

resource "aws_security_group" "lb_http_inbound" {
  name        = "lb-http-inbound"
  description = "Allows HTTP access from safe IP range to Load Balancer"
  vpc_id      = aws_vpc.main.id

  tags = {
    Terraform = "true"
    Project   = "epam-tf-lab"
    Owner     = "${var.student_name}_${var.student_surname}"
    Name      = "lb_http_inbound"
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["185.115.5.79/32"]
  }
}

resource "aws_security_group" "http_inbound" {
  name        = "http-inbound"
  description = "Allows HTTP access from Load Balancer"
  vpc_id      = aws_vpc.main.id

  tags = {
    Terraform = "true"
    Project   = "epam-tf-lab"
    Owner     = "${var.student_name}_${var.student_surname}"
    Name      = "http_inbound"
  }

  ingress {
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [aws_security_group.lb_http_inbound.id]
    description     = "Allow HTTP access from Load Balancer"
  }
}

