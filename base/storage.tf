resource "random_string" "my_numbers" {
  length  = 6
  special = false
  upper   = false
}

resource "aws_s3_bucket" "epam_tf_lab" {
  bucket = "epam-tf-lab-${random_string.my_numbers.result}"

  tags = {
    Terraform = "true"
    Project   = "epam-tf-lab"
    Owner     = "${var.student_name}_${var.student_surname}"
  }
}

resource "aws_s3_bucket_public_access_block" "epam_tf_lab" {
  bucket = aws_s3_bucket.epam_tf_lab.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
