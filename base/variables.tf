variable "student_name" {
  description = "Student's first name"
  type        = string
  default     = "Maksim"
}

variable "student_surname" {
  description = "Student's surname"
  type        = string
  default     = "Medvedev"
}

variable "ssh_key" {
  description = "Provides custom public SSH key"
  type        = string
}
