resource "aws_launch_template" "epam_tf_lab" {
  name_prefix   = "epam-tf-lab-"
  image_id      = "ami-0eaf7c3456e7b5b68"
  instance_type = "t2.micro"
  key_name      = local.key_name

  iam_instance_profile {
    name = local.iam_instance_profile_name
  }

  user_data = base64encode(templatefile("init.sh", { bucket_name = local.s3_bucket_name }))

  network_interfaces {
    associate_public_ip_address = true
    delete_on_termination       = true
    subnet_id                   = data.aws_subnet.public_a.id
    security_groups = [
      data.aws_security_group.ssh_inbound.id,
      data.aws_security_group.http_inbound.id
    ]
  }

  tag_specifications {
    resource_type = "instance"
    tags = {
      Terraform = "true"
      Project   = "epam-tf-lab"
      Owner     = "${var.StudentName}_${var.StudentSurname}"
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "epam_tf_lab" {
  name             = "epam-tf-lab"
  max_size         = 1
  min_size         = 1
  desired_capacity = 1
  vpc_zone_identifier = [
    data.aws_subnet.public_a.id,
    data.aws_subnet.public_b.id,
    data.aws_subnet.public_c.id
  ]

  launch_template {
    id      = aws_launch_template.epam_tf_lab.id
    version = aws_launch_template.epam_tf_lab.latest_version
  }

  tag {
    key                 = "Name"
    value               = "epam-tf-lab"
    propagate_at_launch = true
  }

  lifecycle {
    ignore_changes = [
      load_balancers,
      target_group_arns
    ]
  }
}

resource "aws_elb" "elb_epam_tf_lab" {
  name     = "elb-epam-tf-lab"
  internal = false
  security_groups = [
    data.aws_security_group.lb_http_inbound.id
  ]
  subnets = [
    data.aws_subnet.public_a.id,
    data.aws_subnet.public_b.id,
    data.aws_subnet.public_c.id
  ]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    target              = "HTTP:80/"
    interval            = 30
    timeout             = 5
    healthy_threshold   = 2
    unhealthy_threshold = 2
  }


  tags = {
    Terraform = "true"
    Project   = "epam-tf-lab"
    Owner     = "${var.StudentName}_${var.StudentSurname}"
  }
}

resource "aws_autoscaling_attachment" "asg_attachment" {
  autoscaling_group_name = aws_autoscaling_group.epam_tf_lab.id
  elb                    = aws_elb.elb_epam_tf_lab.id
}
