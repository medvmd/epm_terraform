terraform {
  backend "s3" {
    bucket         = "epam-aws-tf-state-jmfo34ijo34"
    key            = "compute/terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "epam-aws-tf-state-dynamodb-jmfo34ijo34"
  }
}

