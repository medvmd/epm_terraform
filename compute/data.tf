

data "aws_vpc" "main" {
  filter {
    name   = "tag:Name"
    values = ["${var.StudentName}-${var.StudentSurname}-01-vpc"]
  }
}

data "aws_subnet" "public_a" {
  filter {
    name   = "tag:Name"
    values = ["${var.StudentName}-${var.StudentSurname}-01-subnet-public-a"]
  }
}

data "aws_subnet" "public_b" {
  filter {
    name   = "tag:Name"
    values = ["${var.StudentName}-${var.StudentSurname}-01-subnet-public-b"]
  }
}

data "aws_subnet" "public_c" {
  filter {
    name   = "tag:Name"
    values = ["${var.StudentName}-${var.StudentSurname}-01-subnet-public-c"]
  }
}

data "aws_security_group" "ssh_inbound" {
  filter {
    name   = "tag:Name"
    values = ["ssh_inbound"]
  }
}

data "aws_security_group" "http_inbound" {
  filter {
    name   = "tag:Name"
    values = ["http_inbound"]
  }
}

data "aws_security_group" "lb_http_inbound" {
  filter {
    name   = "tag:Name"
    values = ["lb_http_inbound"]
  }
}

locals {
  iam_instance_profile_name = "ec2-instance-profile-for-epam-tf-lab"
  key_name                  = "epam-tf-ssh-key"
  s3_bucket_name            = "epam-tf-lab-8emwhf"
}

data "aws_caller_identity" "current" {} # data.aws_caller_identity.current.account_id
data "aws_region" "current" {}          # data.aws_region.current.name
