resource "aws_iam_role" "test_import" {
  name = "test-import"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Principal = {
          Service = "ec2.amazonaws.com"
        }
        Action = "sts:AssumeRole"
      },
    ]
  })

  tags = {
    Terraform = "true"
    Project   = "epam-tf-lab"
  }
}

