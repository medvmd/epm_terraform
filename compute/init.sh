#!/bin/bash

COMPUTE_MACHINE_UUID=$(sudo cat /sys/devices/virtual/dmi/id/product_uuid | tr '[:upper:]' '[:lower:]')
COMPUTE_INSTANCE_ID=$(curl -s http://169.254.169.254/latest/meta-data/instance-id)

MESSAGE="This message was generated on instance $COMPUTE_INSTANCE_ID with the following UUID $COMPUTE_MACHINE_UUID"

echo "$MESSAGE" | aws s3 cp - s3://${bucket_name}/$COMPUTE_INSTANCE_ID.txt
